#!/usr/bin/perl

# Programmer:   Sevak Deirmendjian
#
# Summary:      This program generates a report to compare and contrast certain features of DNA coding sequence (motif frequency) between multiple organisms.
#
# Input:        (a) console:    L-mer size of motif to use in analysis
#               (b) files:      2 FASTA format files of gene sequences     
#
# Output:       to the console:
#
#               (0) title of the report
#               (1) prompt for user to enter the size of L-mer (between 4 and 8 inclusive)
#               (2) the size of the L-mer
#               (3) the filename of each file opened from inputGeneDirectory
#               (4) the length of DNA sequence of each file in base pairs
#
#               to file output.xls
#
#               (5) the filename
#               (6) the total number of motifs in the file
#               (7) tab delimited headings: rank motif frequency proportion
#               (8) tab delimited top N rankings of the motifs with the highest frequencies of occurences appearing first
#
#
# Status: It works (takes a minute for big FNA files)
#
#                               DATE            ACTION                            NOTE
# Modification History:         11.15.11        initial rough documentation     
#                               11.17.11        calculation for steps (0) to (4)
#                               11.18.11        calculation for steps (5) to (8)
#                                               refined documentation             DONE!


#-----------------------------------------------------------------------

#-----------------------------------------------------------------------

use strict;
use warnings;



#------------------------------------------------------------------
# CONSTANTS
#------------------------------------------------------------------
my $minMotifLen = 4;            # minimum length of motif of interest
my $maxMotifLen = 8;            # maximum length of motif of interest
my $topN = 10;                  # top-N most frequent words

my $dataDirectory = "inputGeneDirectory";       # relative path of directory holding input files		
my $outputFileName = "outputResults.xls";       # set-up the OUTPUT file (tab-delimited for Excel) 
#------------------------------------------------------------------


#------------------------------------------------------------------
# DECLARE DATA STRUCTURES (hash table, arrays, etc)
#------------------------------------------------------------------

my %wordCounts;         # a hash table of frequency counts for each DNA motif
my $sequenceLen;        # length of the DNA sequence from a FNA file
my $nMotifs;            # number of motifs found within the sequence 

my @allWords;           # an array to hold each motif (L-mer)

my $nextWord;           # scalar to hold each word, one at a time
my $countToTop;         # counts up to Top N 
my $lastIndex;          # last index in the array @sorted

my @fileList;           # all files in the directory ($dataDirectory) of data files

my $pathname;           # pathname where FNA files are located
my $filename;           # the names of the FNA files

my $LMER=0;		# size of motif to use in current run
my $LMERlen;            # length of LMER entered; used to catch incorrect input
#------------------------------------------------------------------

# ______(0)_________________________________________________________________
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# print the title

printTitle();	# print the title of the report (to the console)




# ______(1)_________________________________________________________________
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# prompt user to enter size of the LMER

my $ok = 0;  # not ok at start

while ( $ok == 0 ) 
{ # waits for valid entry
 print "What size motif do you want to use (enter an integer between 4 and 8):\n\n";
 $LMER = <STDIN>;
 $LMERlen = length( $LMER );
 if ( $LMER =~ m/\d/ and $minMotifLen <= $LMER and $LMER <= $maxMotifLen and $LMERlen == 2) # checking for digit between 4 and 8; valid entry is 2 characters long (1 integer)
 {
    $ok = 1;
 }
 else
 {
    print "error: not an integer between 4 and 8\n\n";
 } # prints error message for invalid entry
}  # end of while loop waiting for valid entry

# ______(2)____________________________________________________________________
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# prints size of the LMER

print "motif length = $LMER \n\n";

#------------------------------------------------------------------
# establish the absolute pathname to the directory of input files
#------------------------------------------------------------------
# MacOS USERS TAKE NOTE
# determine the absolute pathname to the current directory 
# next two lines need to be uncommented for MacOS X (e.g., BBedit or TexWrangler)
#my @path	= File::Spec->splitdir( File::Spec->rel2abs($0) );
#$pathname	= File::Spec->catfile( @path[0 .. $#path - 1], "");
#------------------------------------------------------------------
# uncomment if using
# WINDOWS or Linux (or MacOS X on command-line)
$pathname   = "";
#------------------------------------------------------------------

#------------------------------------------------------------------
# OPEN the OUTPUT file (tab-delimited for Excel) 
# Define path to output file (we will create this file).

$outputFileName = $pathname."output.xls";

open (OUTPUT, ">", $outputFileName) 
     or die "Cannot open the file: $outputFileName: $!";
#------------------------------------------------------------------


#------------------------------------------------------------------
# read data from each file in the directory
# perform stats for each file

# use filename wildcard (*) to get ALL the files in the directory
$filename = $pathname.$dataDirectory."/*";

# grab a "glob" of files, each of the filenames is put into elements of the array @fileList
@fileList = glob($filename);

my $sequence;           # sequence of DNA from file
my $nextFile;           # name of fna file


#==========================================================================================
# FOREACH file in the directory of input data files
    # (1) read sequence file, merge into one string
    # (2) break sequence into array of L-mers
    # (3) obtain the frequency counts of each word in the file
    # (4) SORT the words in the hash table by frequency count, high counts first
    # (5) Print <TAB>-delimited results for current file into the Excel (output) file 

# _________(3)(5)__________________________________________________________________________
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# prints file name of each file opened from inputGeneDirectory

foreach $nextFile (@fileList)
{
    # print the current filename to the screen (console, stdout)
    print "File Name: $nextFile\n";
    	
    # print filename to Excel (output) file
    print OUTPUT "File Name: $nextFile\n";


    # skip Linux files with names that start with "." 
    if ($nextFile =~ m/^\./)
    {	next;  }
    
    # -----------------------------------------------------------------------------
    # read sequence file, merge into one string
    $sequence = readInDNA($nextFile);
    $sequenceLen = length( $sequence );    # length of sequence
    # _______(gcPercent)___________________________________________________________
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # calculate the GC percentage
    
    my $numG;
    my $numC;
    my $percentG;
    my $percentC;

    $numG = countNucleotides($sequence, "G");
    $numC = countNucleotides($sequence, "C");
    
    print "Number of Gs in the DNA sequence: $numG\n";
    print "Number of Cs in the DNA sequence: $numC\n";
    
    $percentG = ($numG / $sequenceLen) * 100;
    $percentC = ($numC / $sequenceLen) * 100;
    
    printf "Percentage of G: %5.2f %%\n", $percentG;	
    printf "Percentage of C: %5.2f %%\n", $percentC;


    # -----------------------------------------------------------------------------    
    # break sequence into array of L-mers
    @allWords = breakIntoMotifs($sequence, $LMER);


		
    # -----------------------------------------------------------------------------
    # obtain the frequency counts of each word in the file
    
    foreach $nextWord(@allWords)
    {
       $wordCounts{$nextWord}++; # tallies up motifs (keys) and their frequencies (values)
    }   
       
    # -----------------------------------------------------------------------------
  

    # -----------------------------------------------------------------------------
    # SORT the words in the hash table by frequency count, high counts first
    my @sorted; # array holds sorted by tally motifs
    @sorted =  sort { $wordCounts{$b} <=> $wordCounts{$a}
                     	          or $a cmp $b
                    } keys(%wordCounts);
    # -----------------------------------------------------------------------------
	

    # _____________(7)(8)_________________________________________________________________
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Print <TAB>-delimited results for current file into the Excel (output) file 
    
    
    # first print the headers
    print OUTPUT "Rank\tWord\tFrequency\tProportion\n";
    
    $countToTop = 1;                       # counts up to top N
    $nMotifs = $sequenceLen - ($LMER - 1); # number of motifs
    $lastIndex = $#sorted;                 # last index in array @sorted
    while ( $countToTop <= $topN and $countToTop <= $lastIndex + 1)
    { # generates and prints top N motifs
       $nextWord = $sorted[$countToTop-1];
       
       if (exists($wordCounts{$nextWord}))
       { # checks if vallue has a tally
          printf OUTPUT "%d\t%s\t%d\t%1.3f\n", $countToTop, $nextWord, $wordCounts{$nextWord}, $wordCounts{$nextWord}/$nMotifs; # prints rank, word, frequency, and proportion 
       }
       $countToTop++; # increments towards top N
    }
    
 	# -----------------------------------------------------------------------------
 	
 	# make some space in the Excel output between tables
 	print OUTPUT "\n\n\n";
 	



 	# clear the HASH of counts (so we can start over on next file)
 	%wordCounts = ();
 	
} # for each FILE
#==========================================================================================

close(OUTPUT);

print "\n\nDone.\n";




#--------------\
# printTitle    \
#---------------------------------------------------------------------------------
# Subroutine to print the title at the top of the output
#
# RETURNS: nothing

sub printTitle
{
   print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
   print "\t\t Motif Frequency Finder \n";
   print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
   print "\n";
   print "\\   /\\\n";
   print " \\ /  \\  /~~~~~~~~~~~~~\n";    
   print "  \/    \\/  VERSION 5.0\n\n";
   print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n";

}
 
#----- end of printTitle ---------------------------------------------------------


#----------\
# readInDNA \
#---------------------------------------------------------------------------------
# Subroutine to open a FASTA formatted file of DNA sequence, remove
# newline characters at the end of each line, discard any blank lines
# discard lines containing a comment(#) symbol, removes any digits,
# removes any extra whitespace characters, converts nucleotides
# to uppercase, and return as one big string.
#
# 1 argument:  name of file holding the DNA
#
# RETURNS:  file of DNA as one string
#
# A sample of how you might "call" this subroutine from your program
# to 'get' the DNA from a Fasta formatted file for a gene sequence
#
#                  $sequence = readInDNA("humanXM_012345.fna");

sub readInDNA
{
	my $filename = shift;   # name of file
	my $line;               # line being read
	
	my $firstline;          # header line of FNA file
	my $seq;                # DNA sequence from FNA file
	
        # try to open the file; if no file by that name, print a warning and exit
        open (FNAFILE, "<", $filename)
             or die "Cannot open the input file: $filename: $!";
         
        if ( !($firstline = <FNAFILE>) ) {
           print "Can NOT read the header line from the file called: $filename \n";
           exit();
        }
	
	$seq = "";
  
  	# WHILE not EOF, grab next line
	while ($line = <FNAFILE>)		
	{
		chomp $line;
		#print "BEFORE: $line \n";
			
	    	# discard blank line
        	if ($line =~ m/^\s*$/) 
        	{	next; }
        	 
        	# discard comment line (any line with a comment(#) symbol)
        	if ($line =~ m/^\s*#/) 
        	{   next; } 
		
		$line =~ tr/0123456789//d;	# remove all digits
		$line =~ tr/ \t\n\r//d;		# remove any extra whitespace
		$line = uc($line);			# convert everything to upper-case
			
		#print "AFTER: $line \n";
			
    		$seq = $seq . $line;	# concatenate new line onto the entire text so far
	
	} # end WHILE not EOF
	
	close(FNAFILE);
	
	return $seq;
	
} # end subroutine readInDNA
#---------------------------------------------------------------------------------------



#-----------------\
# countNucleotides \
#-----------------------------------------------------------------------
# Subroutine to COUNT and RETURN the number of nucleotides
# in the given sequence.
#
# two arguments:  sequence to search through and the nucleotide to count
#
# RETURNS: number of times the second argument appears in the first arg sequence
#
# A sample of how you might "call" this subroutine from your program:
#
#                  $numberOf_A = countNucleotides("ACGTACGT", "A");
#                  print "$numberOf_A";    # --> would print two(2), two As

sub countNucleotides
{
  my $someSequence = shift(@_); # sequence sent
  my $whatBP = shift(@_); # nucleotide sent

  my $howMany;

  # ignoring case (i), globally (g) find the number of matches (m)
  # of $whatBP that are found in $someSequence
  $howMany = () = $someSequence =~ m/$whatBP/ig ;

  return $howMany;
  
}
#----- end of countNucleotides ---------------------------------------------------
   




#----------------\
# breakIntoMotifs \
#---------------------------------------------------------------------------------------
# Takes a string of DNA ($text),
# and chops it up by a defined LMER length ($LMER),
# and stores each LMER in each cell of an array (@words)
#
# Arguments: 2
# 1. DNA sequence
# 2. LMER length
#
# Returns: array @words which contains the motifs; one word per element
#
#---------------------------------------------------------------------------------------
sub breakIntoMotifs
{

   my $text = shift;           # DNA sequence
   my $LMER = shift;           # LMER length
	
   my $DNAlen = length($text); # length of sequence

   # _________(4)(5)(6)_______________________________________________________________________
   # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
   print "Length of sequence: $DNAlen\n\n";
	
   my @words;	                   # array of words to fill and return when done
	
   my $i=0;	                   # index i points to start of the next word each time
   my $j=0;	                   # j indexes into the array @words; $words[$j] is the next word
   my $nWords = $DNAlen-($LMER-1); # number of motifs
   
   print OUTPUT "Number of motifs: $nWords\n";

   while ($i < $nWords)
   {
      # use substr() to grab the next motif/word;  store in $words[$j]
      $words[$j] = substr($text,$i,$LMER);
      
      # make sure the motif-word does not contain any Ns
      if ( $words[$j] =~ m/N/ )
      {# if it does contain an 'N', skip down the $text beyond the N
         $i++; # move to next motif
      }
      else
      {# if not, increment indices $i and $j
         $i++;
         $j++;
      }	  
    }# while more words
	

    return @words;

} # end subroutine breakIntoMotifs
#-----------------------------------------------------------------------------------------
