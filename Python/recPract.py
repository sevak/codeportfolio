#-------------------------------------------------------------------------------
# Name:        Sevak Deirmendjian
# Purpose:     Recursion Practice
# Created:     27/02/2012
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def fact(n):

    '''a recursive function that calculates factorial of n
    pre: non-negative integer n
    post: factorial of n '''

    if n==0:
        return 1
    else:
        return n * fact(n-1)

def reverse(s):

    '''a recursive function that reverses a string of characters
    pre: a string s
    post: reverse of s'''

    if s == '':
        return s
    else:
        return reverse(s[1:]) + s[0]

def anagrams(s):

    '''a recursive function that generates all anagrams of given string s
    pre: a string s
    post: all anagrams of s'''

    if s == '':
        return [s]
    else:
        ans = []
        for w in anagrams(s[1:]):
            for pos in range(len(w) + 1):
                ans.append(w[:pos] + s[0] + w[pos:])
        return ans

def recPower(a,n):

    '''a recursive function that calculates the result of int a to the int nth power
    pre: an int a
    post: a to to the nth'''

    if n == 0:
        return 1
    else:
        factor = recPower(a, n//2)
        if n%2 == 0:
            return factor * factor
        else:
            return factor * factor * a
    return a

def recBinSearch(x, nums, low, high):

    '''a recursive funtion that searches ordered list nums for target x

    pre: a target x and ordered list nums
    post: index location of target x'''

    if low > high:                     # No place left to look, return -1
        return -1
    mid = (low + high) // 2
    item = nums[mid]
    if item == x:
        return mid                      # Found it! Return the index
    elif x < item:
        return recBinSearch(x, nums, low, high-1)    # Look in lower half
    else:
        return recBinSearch(x, nums, low+1, high)

def search(items, target):

    ''' calls recBinSearch and sets high and low values

    pre: an ordered list of items, a target target
    post: index location of target'''

    return recBinSearch(target, items, 0, len(items))



def main():

    # fact usage examples
    fact4 = fact(4)
    print (fact4)

    fact10 = fact(10)
    print(fact10)

    # reverse usage examples
    reHid = reverse("hideo")
    print(reHid)

    # recPower usage examples
    twoToThird = recPower(2,3)
    print(twoToThird)

    # binary search usage examples
    findOne = search(['a','b','c'],'b')
    findTwo = search([1,2,3],3)
    print(findOne)
    print(findTwo)



if __name__ == '__main__':
    main()
