#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class node{
public:
	node();//constructor
	//data members
	friend class list;
	int data;//data of node
	node *next;//ptr to next node
};
node::node(){//define
	int data = 0;
	node *next = 0;
}

class list{
	friend ostream& operator<<(ostream& out, const list&);
public:
	list();//constructor
	void loadData();//read file to list
	//data members
	node *root;//start of list
	node *tail;//end of list
	int len;//length of list
	//member functions
	int& operator[](const int ind);
	void append(int val);//append value to end of list
};
list::list(){//define
	root = 0;
	tail = new node;
	len = 0;
	void append(int val);//append value to end of list
	void loadData(string fileName);//read file to list	
}

void list::append(int val){
	node *link = new node;//create new node
	link->data = val;//set data of new node as val
	//cout<<link->data<<endl;testing
	if(root==0){
		//cout<<"hi"<<endl;testing
		root = new node;
		root = link;//set root as new node
		tail = link;//update tail
	}
	else{
		if (root->next==0){//if second val
			root->next = link;//set next of root to new node
			tail = link;
		}
		else{
			tail->next = link;//set next of tail to new node
			tail = link;
		}
	}
	len+=1;//update length
	//cout<<len<<endl; testing
}

void list::loadData(){
	int row;
	ifstream input;
	input.open("test.dat");
	if(input.is_open()){
		while(input.good()){//checking state
			input >> row;//read line
			if (input.eof()){break;}
			append(row);//num of line
		}
	input.close();
	} 
	else{cout<<"unable to open file"<<endl;}
}

int& list::operator[](const int ind){
	node *pre = new node;
	pre = root;//copy root
	for(int i=0; i<ind; i++){
		pre = pre->next;
	}
	return pre->data;
}

ostream& operator<<(ostream& out, list& l){
	out<<'[';
	out<<l.root->data;//data of root
	node *pre = new node;
	pre = l.root;//copy root
	for(int i=1; i<l.len; i++){//through list
		pre = pre->next;//climb through pointer
		out<<", "<<pre->data;//data of each node
	}
	out<<']';
	return out;
}
		
	
int main(){
	list nums;
	nums.loadData();//loads file "test.dat"
	cout<<nums<<endl;//testing operator<< overload
	cout<<"index: "<<nums[3]<<endl;//testing operator[] overload
	return 0;
}	
